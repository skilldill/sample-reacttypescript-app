const fs = require('fs')
const { src, dest, parallel } = require('gulp');
const webpack = require('webpack')
const webpackStream = require('webpack-stream')
const sass = require('gulp-sass')
const browser = require('browser-sync')
 
sass.compiler = require('node-sass')

function createStatic(){
	let static = './app/static'
    if(!fs.existsSync(static)){
      	fs.mkdirSync(static);
  	}
}

function js(){
  return src('./src/js/**/*.tsx')
    .pipe(webpackStream({
        output:{
            filename: 'bundle.js'
        },
        resolve: {
          extensions: [".ts", ".tsx", ".js", ".json"]
        },
        module: {
          rules: [
              { test: /\.tsx?$/, loader: "awesome-typescript-loader" },
              { enforce: "pre", test: /\.js$/, loader: "source-map-loader" }
          ]
        },
        mode: 'production',
        externals: {}
    }), webpack)
    .pipe(dest('./app/static/js'))
}

function css(){
  src('./src/css/style.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(dest('./app/static/css/'))
}

exports.js = js;
exports.css = css;
exports.default = parallel(createStatic, js, css);
